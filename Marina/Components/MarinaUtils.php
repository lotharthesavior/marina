<?php

/**
 * MarinaUtils
 *
 * @author Savio Resende <savio@savioresende.com.br>
 *
 * This Class has the responsibility to hold the
 * utility methods to keep original Marina object
 * focused ont eh datainteraction interface
 */

namespace Marina;

abstract class MarinaUtils
{
    /**
     *
     *
     */
    protected $special_characters = [
        "[slash]" => "/"
    ];

    /**
     * Get the current client id, from the session if declared.
     * 
     * @return int
     */
    protected function getCurrentClientId()
    {
        $curret_client_id = $this->config->client_id;
        if (
            isset($_SESSION['user_logon']['ID']) 
            && !empty($_SESSION['user_logon']['ID'])
        ) {
            $curret_client_id = $_SESSION['user_logon']['ID'];
        }

        return $curret_client_id;
    }

    /**
     * Refresh Session
     *
     * Example: {domain}/access_token
     *          Body: grant_type=client_credentials&client_id={client_id}&client_secret={client_secret}&scope={scope}
     *
     * @return string - token of the session
     */
    protected function refresh_session()
    {
        $logon_route = $this->site_url . "/access_token";

        $body = [
            "grant_type" => "client_credentials",
            "client_id" => $this->config->client_id,
            "client_secret" => $this->config->secret_key,
            "scope" => "basic"
        ];

        try {
            $params = [
                'headers' => [
                    "Content-Type" => "application/x-www-form-urlencoded"
                ],
                "form_params" => $body,
                'verify' => false
            ];

            $this->logger->info('Marina Refresh Process - POST ' . $logon_route, $params);

            $response = $this->http->request("POST", $logon_route, $params);

            $contents = $response->getBody()->getContents();

            $result = json_decode($contents);

            $this->logger->info('Marina Refresh Result: ' . $contents);

        } catch (\GuzzleHttp\Exception\RequestException $e) {

            $result = json_decode($e->getResponse()->getBody()->getContents());

        }

        $token = "";
        if (isset($result->access_token)) {
            $token = $result->access_token;
        }

        $_SESSION['session_id'] = $token;

        return $_SESSION['session_id'];
    } // end refresh_session

    /**
     * Convert Standard Report results
     * in Dom Format to php stdClass Object
     *
     * @author Savio Resende
     *
     * @param Object DOM Object
     * @return array - array of stdClass objects
     */
    protected function convertResultDomDocumentToObject($dom)
    {

        $xml_object = $this->xml_to_array($dom);

        $result = "";

        if (isset($xml_object['html'][1]['body']['record'])) {

            $result = $xml_object['html'][1]['body']['record'];

        } else if (isset($xml_object['html'][1]['body']['records'])) {

            $result = $xml_object['html'][1]['body']['records'];

        } else if (isset($xml_object['records'])) {

            $result = $xml_object['records'];

        } else {

            $result = $xml_object;

        }

        // echo "<pre>";var_dump($result);exit;

        return $result;

    } // end convertResultDomDocumentToObject

    /**
     * Convert XML Report into php Array
     *
     * Based on: http://stackoverflow.com/questions/14553547/what-is-the-best-php-dom-2-array-function#14554381
     *
     * @author Savio Resende <savio@savioresende.com.br>
     * @param DOMDocument $root
     * @return Array
     */
    public function xml_to_array($root)
    {
        $result = array();

        if ($root->hasAttributes()) {
            $attrs = $root->attributes;
            foreach ($attrs as $attr) {
                $result['@attributes'][$attr->name] = $attr->value;
            }
        }

        // treating the htmlcontent tags
        if (isset($result['@attributes']["htmlcontent"])) {

            $result['_value'] = $this->DOMinnerHTML($root);

            return $result;
        }

        if ($root->hasChildNodes()) {
            $children = $root->childNodes;
            if ($children->length == 1) {
                $child = $children->item(0);
                if ($child->nodeType == XML_TEXT_NODE) {
                    $result['_value'] = $child->nodeValue;
                    return count($result) == 1
                        ? $result['_value']
                        : $result;
                }
            }
            $groups = array();
            foreach ($children as $child) {
                if (!isset($result[$child->nodeName])) {
                    $result[$child->nodeName] = $this->xml_to_array($child);
                } else {
                    if (!isset($groups[$child->nodeName])) {
                        $result[$child->nodeName] = array($result[$child->nodeName]);
                        $groups[$child->nodeName] = 1;
                    }
                    $result[$child->nodeName][] = $this->xml_to_array($child);
                }
            }
        }

        return $result;
    }

    /**
     * Reference: http://stackoverflow.com/questions/2087103/how-to-get-innerhtml-of-domnode
     */
    private function DOMinnerHTML(\DOMNode $element)
    {
        $innerHTML = "";
        $children = $element->childNodes;

        foreach ($children as $child) {
            $innerHTML .= $element->ownerDocument->saveHTML($child);
        }

        return $innerHTML;
    }

    /**
     * @author Savio Resende
     * @param String $content
     * @internal reference: https://davidwalsh.name/remove-html-comments-php
     */
    protected function remove_html_comments($content = '')
    {
        return preg_replace('/<!--(.|\s)*?-->/', '', $content);
    }

    /**
     * Return the current Session, if it doesn't exist, create
     * one and return it
     *
     * @author Savio Resende
     * @return integer session_id
     */
    public function getSession()
    {
        if (!isset($_SESSION['session_id'])) {
            return $this->refresh_session();
        }

        return $_SESSION['session_id'];
    }

    /**
     * Analyze if exist session
     *
     * @author Savio Resende
     * @return bool
     */
    protected function analyzeSession($result)
    {
        $result_parsed = json_decode($result, true);

        if (
            isset($result_parsed['error'])
            && $result_parsed['error'] == "access_denied"
        ) {
            $this->refresh_session();
            return false;
        }

        return true;
    } // end analyzeSession

    /**
     * Clean the html and body tags, and the name of the report as well
     */
    protected function cleanHTMLWrap($content)
    {
        $content = str_replace("<html>", "", $content);
        $content = str_replace("<body>", "", $content);
        $content = preg_replace('/<!DOCTYPE[^>[]*(\[[^]]*\])?>/', "", $content);
        $content = str_replace("<!--" . $this->report . "-->", "", $content);
        $content = str_replace("</body>", "", $content);
        $content = str_replace("</html>", "", $content);

        return $content;
    }

    /**
     * Clean texts for input
     *
     * Cleaning:
     * Remove line breaks and urlencode for the request body
     *
     * @param String $text
     * @return String
     */
    protected function cleanInputValue($text)
    {
        $text = preg_replace("/\r|\n/", "", $text);
        $text = urlencode($text);
        return $text;
    }

    /**
     * Get the PAGINATION attribute, if it exists, from the report result.
     *
     * . Pages must be in a tag inside the REPORT called "PAGINATION"
     * . User routine > name of Report User Routine: "PAGE_INDEX_URL"
     * . must be inside the condition "if $first_member", to guarantee that it is the first result
     *
     * HOW TO: The procedure to use this method is to run this on the
     * XML result of the search method. This will fill the properties
     * of the model called "pages", and accessed by "$this->pages".
     *
     * @internal this pagination is expecting the current page element
     *           to have a color parameter ("blue"), so the tag will come with
     *           font element in the html.
     * @todo implement next, prev, last, first pages...
     * @param String - XML $report
     */
    protected function handlePagination($report)
    {
        // echo "<pre>";var_dump($report);exit;

        $this->pages = [];

        $this->current_page = null;

        if (
            strpos($report, "<PAGINATION>") !== false
        ) {
            $domain = "http://" . $this->config->site_url . "/" . $this->config->default_params;
            $correct_domain = "http://" . $this->config->site_url . ":8080/" . $this->config->default_params;

            $report = trim($report);

            $first_position = strpos($report, "<PAGINATION>");

            $last_position = strpos($report, "</PAGINATION>");

            $report_slice = substr(
                $report,
                strlen("<PAGINATION>"),
                $last_position - strlen("</PAGINATION>")
            );

            $pages = $report_slice;
            $pages = str_replace("&nbsp;", "", $pages);

            if (!empty($pages)) {

                libxml_use_internal_errors(true);
                $dom = new \DOMDocument();
                $dom->loadHtml($pages);

                foreach ($dom->getElementsByTagName("a") as $element) {

                    // echo "<pre>";var_dump($element->getElementsByTagName('font')[0]->nodeValue);exit;
                    // echo "<pre>";var_dump($element->getElementsByTagName('font')->length);exit;
                    if (
                        $element->getElementsByTagName('font')->length > 0
                        && $element->getElementsByTagName('font')[0]->getAttribute('color') == "blue"
                    ) {
                        // echo "<pre>";var_dump($element->nodeValue);exit;
                        $this->current_page = $element->nodeValue;
                    }
                    // echo "<pre>";var_dump($element->getAttribute(''));exit;
                    // echo "<pre>";var_dump($element);exit;
                    foreach ($element->attributes as $attributes) {
                        $this->pages[$element->nodeValue] = str_replace($domain, $correct_domain, $attributes->value);
                    }

                }

                $report = substr($report, $last_position + strlen("</PAGINATION>"));

            }

            if (is_null($this->current_page)) {
                $this->current_page = 1;
            }
        }

        return $report;
    }

    /**
     * This is considering the report user routine TOTAL_RECORD_COUNT
     *
     * @todo see a better way to treat the get element by tag name,
     *       with lower and upper cases
     * @param String $report - the xml of the report in string format
     */
    protected function handleCountRecords($report)
    {
        // echo "<pre>";var_dump($report);exit;
        if (
            strpos($report, "<RECORD_COUNT>") !== false
        ) {
            libxml_use_internal_errors(true);
            $dom = new \DOMDocument();
            $dom->loadHtml($report);

            $lower_case = "record_count";
            $upper_case = "RECORD_COUNT";

            $result1 = $dom->getElementsByTagName($lower_case);
            $result2 = $dom->getElementsByTagName($upper_case);

            if ($result1->length > 0) {
                foreach ($dom->getElementsByTagName($lower_case) as $element) {
                    $this->record_count = $element->nodeValue;
                }
            } else {
                foreach ($dom->getElementsByTagName($upper_case) as $element) {
                    $this->record_count = $element->nodeValue;
                }
            }
            // echo "<pre>";var_dump($this->record_count);exit;
        }

        return $report;
    }

    /**
     * Handle the user routine PAGE_RECORD_RANGE
     * This method assumes that there is a <PAGE_RECORD_RANGE>
     * tag with this value in it
     *
     * @param String $report - the xml of the report in string format
     */
    public function handlePageRecordRange($report)
    {
        // echo "<pre>";var_dump($report);exit;
        if (
            strpos($report, "<PAGE_RECORD_RANGE>") !== false
        ) {
            libxml_use_internal_errors(true);
            $dom = new \DOMDocument();
            $dom->loadHtml($report);

            $lower_case = "page_record_range";
            $upper_case = "PAGE_RECORD_RANGE";

            $result1 = $dom->getElementsByTagName($lower_case);
            $result2 = $dom->getElementsByTagName($upper_case);

            if ($result1->length > 0) {
                foreach ($dom->getElementsByTagName($lower_case) as $element) {
                    $this->page_record_range = $element->nodeValue;
                }
            } else {
                foreach ($dom->getElementsByTagName($upper_case) as $element) {
                    $this->page_record_range = $element->nodeValue;
                }
            }
            // echo "<pre>";var_dump($this->page_record_range);exit;
        }

        return $report;
    }

    /**
     *
     */
    public function handleSpecialCharacters($inline_data)
    {

        foreach ($this->special_characters as $key => $value) {
            $inline_data = str_replace($key, $value, $inline_data);
        }

        return $inline_data;

    }

    /**
     * Filter the Collection for the current schema
     *
     * @param array $collection
     *
     * @return array $collection
     */
    protected function _filterSchema (array $collection) {
        return array_filter($collection, function($record){
            $valid = true;
            foreach ($this->fields as $key => $field) {
                if (!isset($record->file_content->{$field})) {
                    $valid = false;
                }
            }
            return $valid;
        });
    }

}
