<?php

/**
 * Marina is a Facade with MWI 1
 *
 * Public Methods:
 *  1. getSession
 *  2. search
 *  3. changeSingleRecord
 *  4. deleteRecord
 *  5. unlockRecord
 *
 * Protected Methods:
 *  1. session_search
 *  2. saveRecord
 *
 * Private Methods:
 *  1. refresh_session
 *  2. convertResultDomDocumentToObject
 *  3. processRequestResult
 *  4. analyzeSession
 *
 * @author Savio Resende <savio@savioresende.com.br>
 */

namespace Marina;

use GuzzleHttp\Client;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use PHPUnit\Runner\Exception;

include __DIR__ . '/httpful.phar';
include __DIR__ . '/MarinaUtils.php';

if (session_status() == PHP_SESSION_NONE) {
    @session_start();
}

abstract class Marina extends MarinaUtils
{

    /**
     * Constructor
     *
     * @param Array (
     *  'applicationid' => string (required)
     *  'database' => string (required)
     *  'report' => string (required)
     *  'command' => string
     *  'username' => string
     *  'userpassword' => string
     * )
     *
     * @author Savio Resende
     * @return $this instance
     * @internal config file with json $config
     */
    public function __construct($config_param)
    {

        // initialize Monolog
        $this->logger = new Logger('marina_logger');
        $this->logger->pushHandler(new StreamHandler(__DIR__ . '/../marina.log', Logger::DEBUG));
        $this->logger->pushHandler(new FirePHPHandler());

        $this->database = $config_param['database'];

        $marina_config_file = file_get_contents(__DIR__ . "/../../../../../marina-config.json");

        if (!$marina_config_file) {
            $this->logger->info('Marina error - missing marina-config.json.');
            throw new \Exception("Missing configuration file for your data access.");
        }

        $config = json_decode($marina_config_file);

        $this->config = $config;
        $this->site_url = $config->protocol . "://" . $config->site_url . "/";
        $this->http = new \GuzzleHttp\Client(['base_uri' => $this->site_url]);

        $this->refresh_session();

        return $this;
    }

    /**
     * Sanitize the input data
     *
     * @todo finish this method, maybe change it to MarinaUtils
     * @param Array $input_data
     */
    public function sanitizeInputData(Array $input_data)
    {
        return $input_data;
    }

    /**
     * Validate the input data
     *
     * @todo finish this method, maybe change it to MarinaUtils
     * @param Array $input_data
     */
    public function validateInputData(Array $input_data)
    {
        $missing_fields = [];
        $wrong_type_fields = [];

        // --------------------- required fields ------------------------------
        if (isset($this->required_fields)) {
            foreach ($this->required_fields as $value) {
                if (empty($input_data[$value]) || !isset($input_data[$value])) {
                    array_push($missing_fields, $value);
                }
            }
        }

        if (!empty($missing_fields)) {
            $_SESSION['missing_fields'] = $missing_fields;

            return [
                'status' => false,
                'missing_fields' => $missing_fields
            ];
        }
        // --------------------- / required fields ------------------------------

        // --------------------- field type ------------------------------
        if (isset($this->field_type)) {
            foreach ($input_data as $key => $value) {
                if (isset($this->field_type[$key])) {
                    if (!filter_var($value, $this->field_type[$key])) {
                        array_push($wrong_type_fields, $key);
                    }
                }
            }
        }

        if (!empty($wrong_type_fields)) {
            $_SESSION['wrong_type_fields'] = $wrong_type_fields;
            return [
                'status' => false,
                'wrong_type_fields' => $wrong_type_fields
            ];
        }
        // --------------------- / field type ------------------------------

        return [
            'status' => true
        ];
    }

    /**
     * Search
     *
     * Ex.: URL: http://masadb.dev/{database}/{field}/{value}
     *
     * @param array $params
     * @param bool $recall - specify when the execution is
     *        being done for the first time or not
     *
     * @return array
     */
    public function search(array $params, $recall = false)
    {
        $this->logger->info("Marina - search");

        if (
            !isset($_SESSION['session_id'])
            || !is_numeric($_SESSION['session_id'])
            || empty($_SESSION['session_id'])
        ) {
            $this->refresh_session();
        }

        $search_expression = "";
        foreach ($params as $key => $value) {
            $search_expression .= "/";
            switch ($key) {
                case "id":
                    // $search_expression .= $value;
                    // break;
                default:
                    $search_expression .= $key . "/" . $value;
                    break;
            }
        }

        $route = $this->database . $search_expression;

        try {
            $headers = [
                'Accept' => 'application/json',
                'Authorization' => $_SESSION['session_id'],
                'ClientId' => $this->config->client_id,
                'CurrentClientId' => $this->getCurrentClientId()
            ];
            $this->logger->info('Marina Request - GET ' . $this->site_url . "/" . $route, $headers);
            $response = $this->http->request("GET", $route, [
                'headers' => $headers,
                'verify' => false
            ]);
            $result = json_decode($response->getBody()->getContents());

        } catch (\GuzzleHttp\Exception\RequestException $e) {

            $result = json_decode($e->getResponse()->getBody()->getContents());
            $result = new \Marina\MasaModelResponse($result);

        }

        return $result;
    } // end search

    /**
     * Search POST
     * Ex.: URL: http://masadb.dev/{database}/search
     *
     * @param array $params
     * @param boolean $recall - specify when the execution is
     *        being done for the first time or not
     * @return array
     */
    public function searchRecords(array $params, $recall = false)
    {
        if (!isset($_SESSION['session_id']) || !is_numeric($_SESSION['session_id'])) {
            $this->refresh_session();
        }

        $route = $this->database . "/search";

        $request_params = [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $_SESSION['session_id'],
                'ClientId' => $this->config->client_id,
                'CurrentClientId' => $this->getCurrentClientId()
            ],
            'form_params' => $params,
            'verify' => false
        ];

        $this->logger->info("Marina - searchRecords - POST " . $route, $request_params);

        try {
            $response = $this->http->request("POST", $route, $request_params);

            $contents = $response->getBody()->getContents();

            $result = json_decode($contents);

            $this->logger->info("Marina - searchRecords results: " . $contents);

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            echo $e->getMessage();exit;

            $result = json_decode($e->getResponse()->getBody()->getContents());
            $result = new \Marina\MasaModelResponse($result);
        }

        return $result;
    } // end search

    /**
     * Save the current Record in the Database
     *
     * POST http://masadb/
     *
     * Header -----------
     * Accept: "application/json"
     * Content-Type: "application/x-www-form-urlencoded"
     * ------------------
     *
     * Body ----------------
     * {param}={value}&{param}={value}
     * ---------------------
     *
     * @param array $client_data
     *
     * @return \Marina\MasaModelResponse
     */
    public function save(array $client_data)
    {
        $http_verb = "POST";

        if (!isset($_SESSION['session_id']) || !is_numeric($_SESSION['session_id'])) {
            $this->refresh_session();
        }

        // --- constraints ---
        $constraint_validation_result = $this->validateConstraints($client_data);

        if (
            isset($constraint_validation_result["result"])
            && !$constraint_validation_result["result"]
        ) {
            return $constraint_validation_result;
        }
        // --- / constraints ---

        $route = "/" . $this->database;

        if (
            isset($client_data['id'])
            && !is_null($client_data['id'])
        ) {
            $http_verb = "PUT";
            $route .= "/" . $client_data['id'];
        }

        try {

            $params = [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => $_SESSION['session_id'],
                    'ClientId' => $this->config->client_id,
                    'CurrentClientId' => $this->getCurrentClientId()
                ],
                'form_params' => $client_data,
                'verify' => false
            ];

            $this->logger->info("Marina - Save Request: " . $http_verb . " " . $route, $params);

            $response = $this->http->request($http_verb, $route, $params);

            $response_contents = $response->getBody()->getContents();

            $result = json_decode($response_contents);

        } catch (\GuzzleHttp\Exception\RequestException $e) {

            $result = json_decode($e->getResponse()->getBody()->getContents());

        }

        $this->logger->info("Marina - Save Response ", json_decode($response_contents, true));

        $result = new \Marina\MasaModelResponse($result);

        return $result;
    } // end saveRecord

    /**
     * Validate the income client data based on the model constraints
     *
     * @param array $client_data
     *
     * @return array $client_data || ["result" => bool, "message" => string]
     */
    private function validateConstraints(array $client_data)
    {
        if (!isset($this->constraints)) return true;

        foreach ($this->constraints as $key => $constraint) {

            switch ($constraint) {

                case "unique":
                    if (isset($client_data[$key]) && !isset($client_data['id'])) {
                        $search_result = $this->search([$key => $client_data[$key]]);

                        if (count($search_result->results) > 0) {
                            return [
                                "result" => false,
                                "message" => "error_constraint",
                                "message-detail" => "Already exists a record with " . $key . " '" . $client_data[$key] . "'. "
                            ];
                        }
                    }
                    break;

                case "confirmation":
                    if (isset($client_data[$key]) && isset($client_data[$key . '_confirmation'])) {
                        if ($client_data[$key] !== $client_data[$key . '_confirmation']) {
                            return [
                                "result" => false,
                                "message" => "error_constraint",
                                "message-detail" => $key . " and " . $key . "_confirmation are not equal!"
                            ];
                        }
                    }
                    break;

            }// /switch

        }

        return true;
    }

    /**
     * Delete a Record
     *
     * @param int $id
     * @param bool $after_first_search
     *
     * @return \Marina\MasaModelResponse
     */
    public function deleteRecord($id, $after_first_search = true)
    {
        if (!isset($_SESSION['session_id']) || !is_numeric($_SESSION['session_id'])) {
            $this->refresh_session();
        }

        $route = $this->database . "/" . $id;

        $params = [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $_SESSION['session_id'],
                'ClientId' => $this->config->client_id,
                'CurrentClientId' => $this->getCurrentClientId()
            ],
            'verify' => false
        ];

        $this->logger->info("Marina - Delete Request: DELETE " . $route, $params);

        try {

            $response = $this->http->request("DELETE", $route, $params);
            $result = json_decode($response->getBody()->getContents());

        } catch (\GuzzleHttp\Exception\RequestException $e) {

            $result = json_decode($e->getResponse()->getBody()->getContents());

        }

        $response = new \Marina\MasaModelResponse($result);

        return $response;
    }

    /**
     * Move files present in the Record to the Database
     *
     * @internal This requires for now that the database to be local
     *
     * @param MasaModel $record - it is expected for this record to have "_files"
     *                            attribute through magic method.
     */
    public function moveFilesToDatabase($record)
    {
        $result = false;

        $files_contents = [];

        foreach ($record->_files as $key => $file) {

            // this case considers that the database it is local
            if ($file['file_name']) {

                if ($this->config->localization == "local") {

                    if (isset($this->is_bag) && $this->is_bag) {
                        $origin = 'uploads/' . $file['file_name'];

                        $destination = $this->config->full_address . '/data/'
                            . 'client_' . $this->config->client_id . '/'
                            . $this->database . '/'
                            . $record->_id
                            . '/data/' . $file['file_name'];

                        // copy file
                        $result_ = copy($origin, $destination);

                        // remove local file
                        if ($result_) {
                            $result = $result_;
                            unlink('uploads/' . $file['file_name']);
                        }

                    }

                }

            }

        }

        // $this->updateRecordFilesContent( $record->_id );

        return $result;

    }

    /**
     * Request update of files content in the Record
     *
     * @param Int $record
     */
    private function updateRecordFilesContent($record)
    {
        $this->logger->info("Marina - search");

        if (!isset($_SESSION['session_id']) || !is_numeric($_SESSION['session_id'])) {
            $this->refresh_session();
        }

        $route = $this->database . '/update-record-files-contents/' . $record;

        try {
            $response = $this->http->request("GET", $route, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => $_SESSION['session_id'],
                    'ClientId' => $this->config->client_id,
                    'CurrentClientId' => $this->getCurrentClientId()
                ],
                'verify' => false
            ]);
            $result = json_decode($response->getBody()->getContents());
        } catch (\GuzzleHttp\Exception\RequestException $e) {

            $result = json_decode($e->getResponse()->getBody()->getContents());
            $result = new \Marina\MasaModelResponse($result);

        }

        return $result;
    }

    /**
     * Get file for Download from Database
     *
     * This method depends on \League\Flysystem project.
     *
     * @param Int $record
     * @param String $file_name
     * @param String $extension
     */
    public function getFileInDatabase($record, $file_name, $extension)
    {
        $record_object = $this->searchRecords(['id' => $record]);

        $assets = $record_object->results[0]->file_content->files;
        $assets = array_filter($assets, function ($asset) use ($file_name, $extension) {
            return $asset->file_name == $file_name . "." . $extension;
        });
        // var_dump($assets);exit;

        if (empty($assets)) {
            echo "Asset not found.";
            exit;
        }

        $data_location = $this->config->full_address
            . '/data/client_'
            . $this->config->client_id
            . '/' . $this->database
            . '/' . $record . '/data/';

        $adapter = new \League\Flysystem\Adapter\Local($data_location);
        $filesystem = new \League\Flysystem\Filesystem($adapter);

        ob_clean();
        $name = $data_location . end($assets)->file_name;
        $content = file_get_contents($name);
        $mimetype = $filesystem->getMimetype(end($assets)->file_name);

        // for download
        header('Content-Description: File Transfer');
        header('Content-Type: ' . $mimetype);
        header('Content-Disposition: attachment; filename="' . basename($name) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($name));

        // echo $content;
        readfile($name);

    }

    /**
     *
     */
    protected function clearEmptyFilesArray($client_data){
        if(
            isset($client_data['files'])
            && empty(array_filter($client_data['files']))
        )
            unset($client_data['files']);

        return $client_data;
    }

}

