<?php

namespace Marina;

/**
 * MasaModelResponse
 * 
 * @author Savio Resende <savio@savioresende.com.br>
 */

class MasaModelResponse {

    protected $_result;
    protected $_message;
    protected $_error;

    public function __set($name, $value){
        $this->{$name} = $value;
    }

    public function __get($name){
        return $this->{$name};
    }

    public function __construct( \stdClass $response ){
        $this->_result  = ( isset($response->success) && $response->success ) ? true : false ;

        if( isset($response->successMessage) )
            $this->_message = $response->successMessage;

        if( isset($response->errorMessage) )
            $this->_error = $response->errorMessage;

        if( isset($response->message) ){
            if( $this->_result ){
                $this->_message = $response->message;
            } else {
                $this->_error = $response->message;
            }
        }
    }

}
