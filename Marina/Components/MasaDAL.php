<?php

namespace Marina;

interface MasaDAL {

	/**
	 * Find Record
	 *
	 * @param int $sisn
	 */
	public function find($sisn);

	/**
	 * Find All Records
	 *
	 * @param int $sisn
	 */
	public function findAll();

	/**
	 * Save current Record
	 *
	 * @param String serialized data
	 */
	public function save(Array $client_data);

	/**
	 * Search
	 *
	 * @param Array $client_data
	 * @return Array with result (bool) and message (string)
	 */
	public function search(Array $client_data);

	/**
	 * Validate required fields
	 *
	 * @param Array $client_data
	 */
	public function validate(Array $client_data);
}
