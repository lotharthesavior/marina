<?php

namespace Marina;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;

class MasaController {

	protected $isAjax = false;

    public function __construct(){
        
        $messages_json = file_get_contents("messages.json");
        $this->messages = json_decode($messages_json, true);

    }

	/**
	 * Call in the beggining of every method
	 * 
	 * @param String $method (result of php magic constante __METHOD__)
	 */
	protected function observer( $method, \Slim\Http\Response $response = null ){

		// Create the logger
		$this->logger = new Logger('marina_logger');
		// Now add some handlers
		$this->logger->pushHandler(new StreamHandler(__DIR__.'/../marina.log', Logger::DEBUG));
		$this->logger->pushHandler(new FirePHPHandler());

		$this->logger->info('MasaController - ' . get_class($this) . " - " . $method);

		// --
		// $login_validation = \Helpers\AppHelper::loginValidate();
		$login_validation = 1;
		if( !$login_validation ){
			header("Location: /login");
			exit;
		}

		if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) ){
			$this->isAjax = true;
		}
	}

    /**
     * Convert the given array of files to:
     *     ([
     *         ... ,
     *         'files' => [
     *             [
     *                 'file_name' => string,
     *                 'file_original_name' => string
     *             ]
     *         ]
     *     ])
     *
     * @param Array $post_data
     * @return Array $post_data
     */
    protected function clearFilesParam( Array $post_data )
    {
        if(
            isset($post_data['files'])
            && !empty($post_data['files'])
            && isset($post_data['file_name'])
            && !empty($post_data['file_name'])
            && isset($post_data['file_original_name'])
            && !empty($post_data['file_original_name'])
        ){
            $post_data['files'] = [];
            foreach ($post_data['file_name'] as $key => $file_name) {
                array_push($post_data['files'], [
                    'file_name' => $file_name,
                    'file_original_name' => $post_data['file_original_name'][$key]
                ]);
            }
            unset($post_data['file_name']);
            unset($post_data['file_original_name']);
        }

        return $post_data;
    }

}
