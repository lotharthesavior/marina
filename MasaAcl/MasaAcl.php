<?php

namespace MasaAcl;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Exception\InvalidArgumentException;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;

class MasaAcl extends \Geggleto\Acl\AclRepository
{
    /**
     * 
     * Obs.: To set the page for generic error this method requires the config.json 
     * of the application to contain the template directory, check the minnet
     * project for reference.
     * 
     * @param \Psr\Http\Message\ServerRequestInterface $requestInterface
     * @param \Psr\Http\Message\ResponseInterface      $responseInterface
     * @param callable                                 $next
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke(ServerRequestInterface $requestInterface, ResponseInterface $responseInterface, callable $next) {
        $allowed = false;

        $route = '/' . ltrim($requestInterface->getUri()->getPath(), '/');

        //check to see if the its in the white list
        foreach ($this->whiteList as $whiteUri) {
            if (strpos($route, $whiteUri) !== false) {
                $allowed = true;
            }
        }

       if (!$allowed) {
            try {
                $allowed = $this->isAllowedWithRoles($this->role, $route);
            } catch (InvalidArgumentException $iae) {
                $fn = $this->handler;
                $allowed = $fn($requestInterface, $this);
            }
        }

        if ($allowed) {

            return $next($requestInterface, $responseInterface);

        } else {
            
            return $responseInterface->withStatus(302)->withHeader('Location', '/not-authorized');

        }
    }
}
