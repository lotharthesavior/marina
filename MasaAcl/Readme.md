# Masa ACL Component

Author: Savio Resende <savio@savioresende.com.br>

Component for Access Control Level for Masa Solutions.

Reference: https://github.com/geggleto/geggleto-acl

This project is extended from **geggleto-acl** project in the Reference above.
